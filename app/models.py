
from __future__ import unicode_literals
from datetime import datetime
from django.db import models  # @UnresolvedImport
from django.db.models import Avg


CATEGORY_NATURE = 'Nature'
CATEGORY_ARTISANAT= 'Artisanat'
CATEGORY_GASTRONOMIE = 'Gastronomie'
CATEGORY_PATRIMOINE = 'patrimoine'
CATEGORY_EVENEMENT = 'Evenement'

CATEGORY_VEHICULE = (
    (CATEGORY_NATURE, CATEGORY_NATURE),
    (CATEGORY_GASTRONOMIE, CATEGORY_GASTRONOMIE),
    (CATEGORY_EVENEMENT, CATEGORY_EVENEMENT),
    (CATEGORY_ARTISANAT, CATEGORY_ARTISANAT),
    (CATEGORY_EVENEMENT, CATEGORY_EVENEMENT),
    )

class Departement(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=1500L)
    prefecture = models.CharField(max_length=1150L,blank=True, null=True)
    region = models.CharField(max_length=1150L,blank=True, null=True)
    image = models.CharField(max_length=1150L,blank=True, null=True)

class Tirroir(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=1500L)
    category = models.CharField(max_length=1150L, choices=CATEGORY_VEHICULE, default=CATEGORY_NATURE)
    sub_category = models.CharField(max_length=1150L,blank=True, null=True)
    detail = models.TextField(max_length=1150L,blank=True, null=True)
    photo_id = models.CharField(max_length=1550L)
    photo_list = models.CharField(max_length=1550L)
    photo_detail = models.CharField(max_length=1550L)

    latitude = models.DecimalField(max_digits=12, decimal_places=6)
    longitude = models.DecimalField(max_digits=12, decimal_places=6)
    altitude = models.DecimalField(max_digits=12, default=0, decimal_places=6)

    departement = models.CharField(max_length=1150L)
    code = models.IntegerField(max_length=1150L)
    adresse = models.CharField(max_length=1150L)
    city = models.CharField(max_length=1150L)
    tel = models.CharField(max_length=1150L)
    horaire = models.CharField(max_length=1050L)
    website = models.CharField(max_length=1500L)
    wikipedia = models.CharField(max_length=1050L)
    date_added = models.DateTimeField(auto_now_add=True)


    class Meta:
        db_table = 'tirroir'

    def get_notes(self):
        rating = Note.objects.filter(tirroir = self).aggregate(Avg('value'))
        return rating,Note.objects.filter(tirroir = self).count()

    def get_comments(self):
        return Comment.objects.filter(tirroir = self).count()

    def __str__(self):
        return self.name


class Note(models.Model):
    id = models.AutoField(primary_key=True)
    device = models.CharField(max_length=500L, blank=True, null=True)
    value = models.IntegerField()
    tirroir = models.ForeignKey('Tirroir')
    date_added = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = 'note'

class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    tirroir = models.ForeignKey('Tirroir')
    device = models.CharField(max_length=100L, blank=True, null=True)
    text = models.CharField(max_length=500L, blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = 'Comment'

class AuthGroup(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80L, unique=True)

    class Meta:
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.AutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')

    class Meta:
        db_table = 'auth_group_permissions'


class AuthPermission(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50L)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100L)

    class Meta:
        db_table = 'auth_permission'


class AuthUser(models.Model):
    id = models.AutoField(primary_key=True)
    password = models.CharField(max_length=128L)
    last_login = models.DateTimeField()
    is_superuser = models.IntegerField()
    username = models.CharField(max_length=30L, unique=True)
    first_name = models.CharField(max_length=30L)
    last_name = models.CharField(max_length=30L)
    email = models.CharField(max_length=75L)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        db_table = 'auth_user'

    def __unicode__(self):
        return "{0} {1}".format(self.first_name, self.last_name)


class AuthUserGroups(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(AuthUser)
    group = models.ForeignKey(AuthGroup)

    class Meta:
        db_table = 'auth_user_groups'


class AuthUserUserPermissions(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(AuthUser)
    permission = models.ForeignKey(AuthPermission)

    class Meta:
        db_table = 'auth_user_user_permissions'


class DjangoContentType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100L)
    app_label = models.CharField(max_length=100L)
    model = models.CharField(max_length=100L)

    class Meta:
        db_table = 'django_content_type'


class DjangoSession(models.Model):
    session_key = models.CharField(max_length=40L, primary_key=True)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        db_table = 'django_session'

class DjangoSite(models.Model):
    id = models.AutoField(primary_key=True)
    domain = models.CharField(max_length=100L)
    name = models.CharField(max_length=50L)

    class Meta:
        db_table = 'django_site'

