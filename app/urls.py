from app import views
from rest_framework import routers  # @UnresolvedImport


router = routers.DefaultRouter()
router.register(r'tirroir', views.TirroirView, base_name='api_tirroir')
router.register(r'departement', views.DepartementView, base_name='api_departement')
router.register(r'tirroir/(?P<tirroir>[-\w]+)/comments', views.CommentView, base_name='api_comment')
router.register(r'tirroir/(?P<tirroir>[-\w]+)/notes', views.NoteView, base_name='api_note')
urlpatterns = router.urls