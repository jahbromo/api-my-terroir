from app import models
from rest_framework import serializers

               
class TirroirSerializer(serializers.ModelSerializer):
    note = serializers.ReadOnlyField(source='get_notes')
    comment = serializers.ReadOnlyField(source='get_comments')
    class Meta:
        model = models.Tirroir

class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Note

class DepartementSerialiser(serializers.ModelSerializer):
    class Meta:
        model = models.Departement

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Comment
    
