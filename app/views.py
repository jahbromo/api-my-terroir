from app import serializer
from app import models
from rest_framework import serializers
from rest_framework import viewsets

class TirroirView(viewsets.ModelViewSet):

    serializer_class = serializer.TirroirSerializer
    queryset = models.Tirroir.objects.all()

class DepartementView(viewsets.ModelViewSet):
    serializer_class = serializer.DepartementSerialiser
    queryset = models.Departement.objects.all()

class CommentView(viewsets.ModelViewSet):
    serializer_class = serializer.CommentSerializer

    def get_queryset(self):
        tirroir = models.Tirroir.objects.get(pk=self.kwargs['tirroir'])
        return  models.Comment.objects.filter(tirroir=tirroir)

class NoteView(viewsets.ModelViewSet):
    serializer_class = serializer.NoteSerializer

    def get_queryset(self):
        tirroir = models.Tirroir.objects.get(pk=self.kwargs['tirroir'])
        return  models.Note.objects.filter(tirroir=tirroir)